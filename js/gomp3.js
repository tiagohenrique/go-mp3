/*jslint browser: true, regexp: true*/
/*global $, FormGo, console*/

var FormGo = function () {
    'use strict';

    this.init = function () {
        $("#btn-prepare").on("click", function (e) {
            e.preventDefault();
            var url, urlPrev, btnDownload = $("#btn-download"), player = $("#player"), source;

            url = $("#formGo #url").val();
            urlPrev = btnDownload.attr("href");

            if (url !== urlPrev) {
                btnDownload.attr("href", url);
            }
            
            source = $(document.createElement("source"));
            source.attr({
                type: "audio/mpeg",
                src: url
            });
            
            player.append(source);
            
            $("#download").removeClass("hidden");
        });

        $("#btn-download").on("click", function (e) {});
    };

    return this;
};