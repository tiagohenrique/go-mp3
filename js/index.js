/*jslint browser: true*/
/*global $, FormGo*/

$(document).ready(function () {
    'use strict';
    
    var formGo = new FormGo();
    formGo.init();
});
